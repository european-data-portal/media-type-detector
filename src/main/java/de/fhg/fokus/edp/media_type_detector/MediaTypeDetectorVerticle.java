package de.fhg.fokus.edp.media_type_detector;

import de.fhg.fokus.edp.media_type_detector.model.ArchiveType;
import de.fhg.fokus.edp.media_type_detector.model.MediaTypeDetectRequest;
import de.fhg.fokus.edp.media_type_detector.model.MediaTypeDetectResponse;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.client.WebClient;
import org.apache.commons.io.FilenameUtils;
import org.apache.tika.Tika;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import static de.fhg.fokus.edp.media_type_detector.Constants.ENV_WORK_DIR;
import static de.fhg.fokus.edp.media_type_detector.model.ArchiveType.NONE;


public class MediaTypeDetectorVerticle extends AbstractVerticle {

    private static final Logger LOG = LoggerFactory.getLogger(MediaTypeDetectorVerticle.class);

    private WebClient webClient;
    private Tika tika;

    @Override
    public void start(Future<Void> future) {

        webClient = WebClient.create(vertx);
        tika = new Tika();

        vertx.eventBus().consumer(Constants.ADDRESS_CHECK_MEDIA_TYPE, this::handleMimeTypeCheckRequest);

        future.complete();
    }

    private void handleMimeTypeCheckRequest(Message<String> message) {
        MediaTypeDetectRequest mediaTypeDetectRequest = Json.decodeValue(message.body(), MediaTypeDetectRequest.class);
        LOG.debug("Received check request: {}", mediaTypeDetectRequest.toString());

        List<MediaTypeDetectResponse> mediaTypes = new ArrayList<>();
        vertx.executeBlocking(handler -> {

            mediaTypeDetectRequest.getFiles().forEach(file -> {
                String mediaType = getMediaType(file);
                LOG.debug("MediaType of [{}] is [{}]", file, mediaType);

                ArchiveType archiveType = fromMediaType(mediaType);

                if (archiveType.equals(NONE)) {
                    mediaTypes.add(new MediaTypeDetectResponse(file.getFileName().toString(), mediaType, false));
                } else {
                    try {
                        Path outputDir = Paths.get(config().getString(ENV_WORK_DIR, "/tmp"), FilenameUtils.getBaseName(file.toString()));

                        ArchiveUtil.extract(file, outputDir, archiveType).forEach(archiveFile -> {
                            // remove base path from filenames
                            String sanitizedPath = outputDir.relativize(archiveFile).toString();

                            mediaTypes.add(new MediaTypeDetectResponse(sanitizedPath, getMediaType(archiveFile), true));
                        });

                    } catch (IOException e) {
                        e.printStackTrace();
                        LOG.error("Failed to extract [{}]: ", file, e);
                        mediaTypes.add(new MediaTypeDetectResponse(file.getFileName().toString(), mediaType, false));
                    }
                }
            });

            handler.complete();

        }, result -> {
            cleanUp(mediaTypeDetectRequest.getFiles());
            sendResponse(mediaTypes, mediaTypeDetectRequest.getCallbackUrl());
        });
    }

    private String getMediaType(Path path) {
        return tika.detect(path.toString());
    }

    private void sendResponse(List<MediaTypeDetectResponse> result, String url) {
        JsonObject json = new JsonObject()
                .put("success", true)
                .put("result", result);

        LOG.debug("Attempting to send response {} to [{}]", json, url);

        webClient.postAbs(url)
                .sendJsonObject(json, handler -> {
                    if (handler.failed())
                        LOG.error("Failed to send response to [{}]: {}", url, handler.cause());
                });
    }

    private void cleanUp(List<Path> files) {
        files.forEach(path ->
            vertx.fileSystem().delete(path.toString(), handler -> {
                if (handler.failed())
                    LOG.error("Failed to delete file [{}]", path);
            }));
    }

    private ArchiveType fromMediaType(String mediaType) {
        for (ArchiveType archiveType : ArchiveType.values())
            if (archiveType.getMediaType().equals(mediaType))
                return archiveType;

        return NONE;
    }
}
