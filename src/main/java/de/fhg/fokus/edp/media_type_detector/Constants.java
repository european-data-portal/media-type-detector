package de.fhg.fokus.edp.media_type_detector;

final class Constants {

    static final String ENV_APPLICATION_PORT = "PORT";
    static final String ENV_WORK_DIR = "WORK_DIR";

    static final String ADDRESS_CHECK_MEDIA_TYPE = "detect";

    static final String FORM_PARAM_CALLBACK_URL = "callbackUrl";
}
