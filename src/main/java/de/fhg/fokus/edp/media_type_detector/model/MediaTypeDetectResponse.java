package de.fhg.fokus.edp.media_type_detector.model;

public class MediaTypeDetectResponse {

    private String fileName;
    private String mediaType;
    private Boolean isCompressed;

    public MediaTypeDetectResponse() {
    }

    public MediaTypeDetectResponse(String fileName, String mediaType, Boolean isCompressed) {
        this.fileName = fileName;
        this.mediaType = mediaType;
        this.isCompressed = isCompressed;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getMediaType() {
        return mediaType;
    }

    public void setMediaType(String mediaType) {
        this.mediaType = mediaType;
    }

    public Boolean getCompressed() {
        return isCompressed;
    }

    public void setCompressed(Boolean compressed) {
        isCompressed = compressed;
    }

    @Override
    public String toString() {
        return "MediaTypeDetectResponse{" +
                "fileName='" + fileName + '\'' +
                ", mediaType='" + mediaType + '\'' +
                ", isCompressed=" + isCompressed +
                '}';
    }
}
