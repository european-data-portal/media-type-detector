package de.fhg.fokus.edp.media_type_detector.model;

import java.nio.file.Path;
import java.util.List;

public class MediaTypeDetectRequest {

    private String callbackUrl;
    private List<Path> files;

    public String getCallbackUrl() {
        return callbackUrl;
    }

    public void setCallbackUrl(String callbackUrl) {
        this.callbackUrl = callbackUrl;
    }

    public List<Path> getFiles() {
        return files;
    }

    public void setFiles(List<Path> files) {
        this.files = files;
    }

    @Override
    public String toString() {
        return "MediaTypeDetectRequest{" +
                "callbackUrl='" + callbackUrl + '\'' +
                ", files=" + files +
                '}';
    }
}
