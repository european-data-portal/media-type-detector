package de.fhg.fokus.edp.media_type_detector;

import de.fhg.fokus.edp.media_type_detector.model.ArchiveType;
import io.vertx.junit5.VertxExtension;
import org.apache.commons.io.FileUtils;
import org.apache.tika.Tika;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static de.fhg.fokus.edp.media_type_detector.model.ArchiveType.*;

@ExtendWith(VertxExtension.class)
class ArchiveTest {

    private static Path ZIP_ARCHIVE;
    private static Path TAR_ARCHIVE;
    private static Path TAR_GZ_ARCHIVE;

    private static Path ZIP_OUTPUT_DIR;
    private static Path TAR_OUTPUT_DIR;
    private static Path TAR_GZ_OUTPUT_DIR;

    private static List<String> ARCHIVE_FILES = Arrays.asList("sample_1", "sample_2", "sample_3");

    @BeforeAll
    static void setup() {
        try {
            ClassLoader loader = ArchiveTest.class.getClassLoader();

            ZIP_ARCHIVE = Paths.get(loader.getResource("test.zip").toURI());
            TAR_ARCHIVE = Paths.get(loader.getResource("test.tar").toURI());
            TAR_GZ_ARCHIVE = Paths.get(loader.getResource("test.tar.gz").toURI());

            ZIP_OUTPUT_DIR = Files.createTempDirectory("media-type-detector-test_").resolve("zipTest");
            TAR_OUTPUT_DIR = Files.createTempDirectory("media-type-detector-test_").resolve("tarTest");
            TAR_GZ_OUTPUT_DIR = Files.createTempDirectory("media-type-detector-test_").resolve("tarGzTest");
        } catch (Exception e) {
            fail(e);
        }
    }

    @AfterAll
    static void tearDown() {
        try {
            FileUtils.deleteDirectory(ZIP_OUTPUT_DIR.toFile());
            FileUtils.deleteDirectory(TAR_OUTPUT_DIR.toFile());
            FileUtils.deleteDirectory(TAR_GZ_OUTPUT_DIR.toFile());
        } catch (Exception e) {
            fail(e);
        }
    }

    @Test
    void testMediaTypes() {
        Tika tika = new Tika();
        try {
            assertEquals(ZIP.getMediaType(), tika.detect(ZIP_ARCHIVE));
            assertEquals(TAR.getMediaType(), tika.detect(TAR_ARCHIVE));
            assertEquals(TAR_GZ.getMediaType(), tika.detect(TAR_GZ_ARCHIVE));
        } catch (Exception e) {
            fail(e);
        }
    }

    @Test
    void testZipExtraction() {
        testArchiveContents(ZIP_ARCHIVE, ZIP_OUTPUT_DIR, ZIP);
    }

    @Test
    void testTarExtraction() {
        testArchiveContents(TAR_ARCHIVE, TAR_OUTPUT_DIR, TAR);
    }

    @Test
    void testTarGzExtraction() {
        testArchiveContents(TAR_GZ_ARCHIVE, TAR_GZ_OUTPUT_DIR, TAR_GZ);
    }

    private void testArchiveContents(Path archive, Path outputDir, ArchiveType type) {
        try {
            List<Path> extractedFiles =
                    ArchiveUtil.extract(archive, outputDir, type);

            assertEquals(ARCHIVE_FILES.size(), extractedFiles.size());
            extractedFiles.forEach(file ->
                    assertTrue(ARCHIVE_FILES.contains(file.getFileName().toString())));

        } catch (IOException e) {
            fail(e);
        }
    }
}
